import dynamic from 'next/dynamic';
const ReactPlayer = dynamic(() => import('react-player'), { ssr: false });

export default function Player(_url) {

    return  (<ReactPlayer
                url={_url._url}
                controls 
                
            />)
}