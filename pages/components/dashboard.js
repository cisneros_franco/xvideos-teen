'use client'
import axios from 'axios';
import * as XLSX from 'xlsx';
import dynamic from 'next/dynamic';
const ReactPlayer = dynamic(() => import('react-player'), { ssr: false });
import styles from '../../styles/Home.module.css'
import { useCallback, useEffect, useState } from 'react';
import Player from './Player';

function Dashboard(_videos, scrollUp) {
    const googleDriveLink = 'https://docs.google.com/spreadsheets/d/1393hDIlEQESvipc_SWCt8m8Tzqbr9sO_/edit?usp=sharing&ouid=111501582808999198395&rtpof=true&sd=true'; // Reemplaza esto con tu URL  
    const [listaVideos, setListaVideos] = useState(null);
    const [videosFiltrados, setVideosFiltrados] = useState(null);
    const [videoSelecionado, setVideoSelecionado] = useState([]);
    const [categorias, setCategorias] = useState([]);
    const [repFlag, setrepFlag] = useState(false);

    useEffect(() => {
        const fetchData = async () => {
          try {
            const response = await axios.get(googleDriveLink, {
              responseType: 'blob', // Importante para obtener el archivo binario
            });
    
            const blob = response.data;
            const reader = new FileReader();
            reader.onload = () => {
              const fileData = reader.result;
              const workbook = XLSX.read(fileData, { type: 'binary' });
              const sheetName = workbook.SheetNames[0];
              const sheet = workbook.Sheets[sheetName];
              const jsonData = XLSX.utils.sheet_to_json(sheet, { header: ['numerador', 'id', 'usuario', 'titulo', 'categoria', 'url']});
              const elementos = [];
              const filtroCategorias = [];
              for(let index = 2; index < jsonData.length; index++) {
                index=index+1; 
                jsonData.forEach(e => {
                    if(e.numerador>1 && e.titulo!=undefined) {
                        if(!elementos.find(i => i.id==e.id)){
                            elementos.push(e);
                        }
                        if(!filtroCategorias.find(I => I.categoria===e.categoria)) {
                            filtroCategorias.push({categoria: e.categoria});
                        }
                    }
                });
              }
              setListaVideos(elementos);
              setVideosFiltrados(elementos);
              setCategorias(filtroCategorias);
            };  
            reader.readAsBinaryString(blob);

        } catch (error) {
          console.error('Error al leer el archivo Excel:', error);
        }
      };
      fetchData();
    }, []);

    const handleFilter = useCallback((_cat) => {
        let data = [];
        if(_cat==="ALL") {
            setVideosFiltrados(listaVideos);
        }
        else if (listaVideos!=undefined && listaVideos.length>0) {
            data = listaVideos.filter(e => e.categoria==_cat);
            setVideosFiltrados(data);
            setrepFlag(false);
        }
        
    }, [listaVideos])

    const handleChangeVideo = useCallback(async valor => {   
        const elemento = videosFiltrados.find(e => e.id===valor)
        setVideoSelecionado(elemento);
        if(elemento!=null) {
            setrepFlag(true);
        }
       scrollTo(0, 0);
    },[videosFiltrados, videoSelecionado]);



    return(
        <div className={styles.dashboard} >
            {
                repFlag===true &&
                <div className={styles.reproductor}>
                    {
                        videoSelecionado!=null && <Player _url={videoSelecionado!=null && videoSelecionado.url}/>
                    }        
                </div>
            }
            
            <div className={styles.navegation}>
                
                {
                    categorias.map(i => <p key={i.categoria} className={styles.navItem} onClick={() => handleFilter(i.categoria)}
                        >{i.categoria}
                    </p>)
                }
                {
                    categorias.length>0 && <p className={styles.navItem} onClick={() => handleFilter('ALL')}>TODOS</p>
                }
            </div>
            <div className={styles.listaVideos}>
                {videosFiltrados!=null && videosFiltrados.map(i => 
                    <div key={i.id} className={styles.listaItem}>
                        <div className={styles.listaPlayer}>
                            <ReactPlayer 
                                url={i.url} 
                                width='100%' 
                                height='100%' 
                                margin='0' 
                                muted='1'
                                onClick={() => handleChangeVideo(i.id)}
                                />
                        </div> 
                        <div className={styles.creditos}>
                            <p className={styles.usuario}>{i.usuario}</p>
                            <p className={styles.titulo}>{i.titulo}</p>
                        </div>
                    </div>  
                    )
                }
            </div>
        </div>
    )
}
export default Dashboard;